"""Positive and negative unit test cases."""
import pathlib
import subprocess
import sys
import unittest
from main import WordProcessor  # pylint: disable=wrong-import-position

REPO_ROOT = pathlib.Path(__file__).parent

class WordProcessTestCases(unittest.TestCase):
    """
    Unit Test Cases for testing word process
    """

    def test_process_word(self):
        """Verify the process_word function."""
        # given
        word = 'myword'
        processor = WordProcessor()

        # exercise
        processor.process_word(word)

        # verify
        self.assertEqual(len(processor.words), 1)
        self.assertEqual(processor.words[0], 'myword')
        self.assertEqual(processor.word_length, 6)

    def test_process_word_with_numeric(self):
        """Verify the process_word function with only numeric characters."""
        # given
        word = '12345'
        processor = WordProcessor()

        # exercise
        processor.process_word(word)

        # verify
        self.assertEqual(len(processor.words), 1)
        self.assertEqual(processor.words[0], '12345')
        self.assertEqual(processor.word_length, 5)

    def test_words_from_file(self):
        """Verify processing from a file."""
        # given
        filepath = REPO_ROOT/'data'/'file_with_words.txt'
        processor = WordProcessor()

        # exercise
        processor.process_file(filepath)

        # verify
        self.assertEqual(len(processor.words), 1)

    def test_command_process(self):
        """Verify calling this script from the command line works."""
        # given
        filepath = REPO_ROOT/'data'/'file_with_words.txt'
        cmd_args = [sys.executable, 'main.py', '--filepath', filepath]

        # exercise
        output = subprocess.check_output(cmd_args, cwd=REPO_ROOT)

        # verify
        self.assertIsNotNone(output)

    def test_process_word_word_is_skipped(self):
        """This negative test will verify that words with special characters are skipped."""
        # given
        word = 'mywo$rd'
        processor = WordProcessor()

        # exercise
        processor.process_word(word)

        # verify
        self.assertEqual(len(processor.words), 0)
        self.assertEqual(processor.word_length, 0)

    def test_no_file(self):
        """This negative test will verify an Exception is thrown if the file is not found."""
        # given
        filepath = 'data/noexist'
        processor = WordProcessor()

        # exercise, verify
        self.assertRaises(FileNotFoundError, processor.process_file, filepath)

    def test_special_characters_file(self):
        """This negative test will verify file with no alpha numeric characters is handled"""
        # given
        filepath = REPO_ROOT/'data'/'special_chars_file.txt'
        processor = WordProcessor()

        # exercise
        processor.process_file(filepath)

        # verify
        self.assertEqual(len(processor.words), 0)

    def test_empty_file(self):
        """This negative test will verify if the empty file is handled"""
        # given
        filepath = REPO_ROOT/'data'/'empty_file.txt'
        processor = WordProcessor()

        # exercise, verify
        self.assertRaises(ValueError, processor.process_file, filepath)

if __name__ == '__main__':
    unittest.main()
