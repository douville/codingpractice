### To execute the program

Clone this git repository to local machine

Execute file main.py file from IDE or Terminal

```bash
python3 main.py
```

### To execute tests

tests are in tests.py, execute them by using following command

```bash
python3 tests.py
```

### Test Data

* Test data files are in data folder

* For demo purpose, I provided file_with_words.txt as default input, user can provide a different input by file path

* I used empty_fie.txt and special_chars_file.txt for unit tests

* I am reading line by line to prevent loading the entire file into memory (handling large input files)


### ASSUMPTIONS

* Words are separated by white spaces

* Words containing special characters are skipped in the script assuming that all words are alpha numeric

* Question did not mention about input file size, Script is  reading test data line by line instead of loading entire file into memory, to handle large files too

* Script is handling transpose a word as reverse operation as provided by the example in question

* Question did not mention about same length long words, Script handles multiple same length long words


### Test Cases

* Tested process_word method if it returns a proper word

* Tested program with no file input to verify required error type

* Tested process_file method to check if it is handling file path properly

* Tested subprocess commands to check if it is handling command line input properly

* Tested process_word method to check if it is handling special characters

* Tested process_word method to check if it is handling only numeric characters

* Tested program with a file that does not have any proper words

* Tested program with an empty file to check if it is throwing correct error type


### Execution gif

![Execution](https://gitlab.com/pradeepcsu/codingpractice/blob/master/Program_Execution.gif)
